﻿namespace MissingGondoldaApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.SystemsPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.FindGondolaFilesBtn = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.ProgressLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
			this.BottomPanel = new System.Windows.Forms.Panel();
			this.LogList = new System.Windows.Forms.ListBox();
			this.ContentPanel = new System.Windows.Forms.Panel();
			this.Grid = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewButtonColumn();
			this.MagGrid = new System.Windows.Forms.DataGridView();
			this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.GridLogSplitContainer = new System.Windows.Forms.SplitContainer();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.BottomPanel.SuspendLayout();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MagGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLogSplitContainer)).BeginInit();
			this.GridLogSplitContainer.Panel1.SuspendLayout();
			this.GridLogSplitContainer.Panel2.SuspendLayout();
			this.GridLogSplitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.SystemsPanel);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(758, 29);
			this.panel1.TabIndex = 3;
			// 
			// SystemsPanel
			// 
			this.SystemsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SystemsPanel.Location = new System.Drawing.Point(0, 0);
			this.SystemsPanel.Name = "SystemsPanel";
			this.SystemsPanel.Size = new System.Drawing.Size(596, 29);
			this.SystemsPanel.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.FindGondolaFilesBtn);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(596, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(162, 29);
			this.panel2.TabIndex = 2;
			// 
			// FindGondolaFilesBtn
			// 
			this.FindGondolaFilesBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.FindGondolaFilesBtn.Location = new System.Drawing.Point(8, 3);
			this.FindGondolaFilesBtn.Name = "FindGondolaFilesBtn";
			this.FindGondolaFilesBtn.Size = new System.Drawing.Size(151, 23);
			this.FindGondolaFilesBtn.TabIndex = 1;
			this.FindGondolaFilesBtn.Text = "Find Gondola Files";
			this.FindGondolaFilesBtn.UseVisualStyleBackColor = true;
			this.FindGondolaFilesBtn.Click += new System.EventHandler(this.FindGondolaFiles_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressLabel,
            this.ProgressBar});
			this.statusStrip1.Location = new System.Drawing.Point(4, 543);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(758, 22);
			this.statusStrip1.TabIndex = 4;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// ProgressLabel
			// 
			this.ProgressLabel.Name = "ProgressLabel";
			this.ProgressLabel.Size = new System.Drawing.Size(541, 17);
			this.ProgressLabel.Spring = true;
			this.ProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ProgressBar
			// 
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(200, 16);
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.LogList);
			this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BottomPanel.Location = new System.Drawing.Point(0, 0);
			this.BottomPanel.Name = "BottomPanel";
			this.BottomPanel.Size = new System.Drawing.Size(758, 81);
			this.BottomPanel.TabIndex = 5;
			// 
			// LogList
			// 
			this.LogList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LogList.FormattingEnabled = true;
			this.LogList.IntegralHeight = false;
			this.LogList.Location = new System.Drawing.Point(0, 0);
			this.LogList.Name = "LogList";
			this.LogList.Size = new System.Drawing.Size(758, 81);
			this.LogList.TabIndex = 0;
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.Grid);
			this.ContentPanel.Controls.Add(this.MagGrid);
			this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ContentPanel.Location = new System.Drawing.Point(0, 0);
			this.ContentPanel.Name = "ContentPanel";
			this.ContentPanel.Size = new System.Drawing.Size(758, 425);
			this.ContentPanel.TabIndex = 6;
			// 
			// Grid
			// 
			this.Grid.AllowUserToAddRows = false;
			this.Grid.AllowUserToDeleteRows = false;
			this.Grid.AllowUserToOrderColumns = true;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
			this.Grid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Grid.Location = new System.Drawing.Point(0, 0);
			this.Grid.MultiSelect = false;
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.Grid.ShowCellErrors = false;
			this.Grid.ShowEditingIcon = false;
			this.Grid.ShowRowErrors = false;
			this.Grid.Size = new System.Drawing.Size(643, 425);
			this.Grid.TabIndex = 3;
			this.Grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellContentClick);
			this.Grid.SelectionChanged += new System.EventHandler(this.Grid_SelectionChanged);
			this.Grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grid_KeyDown);
			// 
			// Column1
			// 
			this.Column1.FillWeight = 50F;
			this.Column1.HeaderText = "PP";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.ToolTipText = "PP Line this file was found on";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Gondola";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.ToolTipText = "Gondola #";
			// 
			// Column3
			// 
			this.Column3.FillWeight = 150F;
			this.Column3.HeaderText = "Fill Start";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.ToolTipText = "Gondola Fill Start Time";
			// 
			// Column4
			// 
			this.Column4.FillWeight = 150F;
			this.Column4.HeaderText = "Fill End";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.ToolTipText = "Gondola Fill End Time";
			// 
			// Column5
			// 
			this.Column5.FillWeight = 50F;
			this.Column5.HeaderText = "# Mags";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.ToolTipText = "Count of Magazines assigned to this Gondola";
			// 
			// Column6
			// 
			this.Column6.FillWeight = 75F;
			this.Column6.HeaderText = "Status";
			this.Column6.Name = "Column6";
			this.Column6.ReadOnly = true;
			this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.Column6.Text = "";
			this.Column6.ToolTipText = "If this file has been pushed to SCADA or not";
			// 
			// MagGrid
			// 
			this.MagGrid.AllowUserToAddRows = false;
			this.MagGrid.AllowUserToDeleteRows = false;
			this.MagGrid.AllowUserToOrderColumns = true;
			this.MagGrid.AllowUserToResizeRows = false;
			this.MagGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
			this.MagGrid.ColumnHeadersHeight = 34;
			this.MagGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.MagGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8});
			this.MagGrid.Dock = System.Windows.Forms.DockStyle.Right;
			this.MagGrid.Location = new System.Drawing.Point(643, 0);
			this.MagGrid.MultiSelect = false;
			this.MagGrid.Name = "MagGrid";
			this.MagGrid.ReadOnly = true;
			this.MagGrid.RowHeadersVisible = false;
			this.MagGrid.RowTemplate.Height = 18;
			this.MagGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.MagGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.MagGrid.ShowCellErrors = false;
			this.MagGrid.ShowEditingIcon = false;
			this.MagGrid.ShowRowErrors = false;
			this.MagGrid.Size = new System.Drawing.Size(115, 425);
			this.MagGrid.TabIndex = 4;
			this.MagGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MagGrid_KeyDown);
			// 
			// Column7
			// 
			this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			this.Column7.HeaderText = "Mag#";
			this.Column7.MinimumWidth = 40;
			this.Column7.Name = "Column7";
			this.Column7.ReadOnly = true;
			this.Column7.Width = 40;
			// 
			// Column8
			// 
			this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			this.Column8.HeaderText = "Lot#";
			this.Column8.MinimumWidth = 65;
			this.Column8.Name = "Column8";
			this.Column8.ReadOnly = true;
			this.Column8.Width = 65;
			// 
			// GridLogSplitContainer
			// 
			this.GridLogSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridLogSplitContainer.Location = new System.Drawing.Point(4, 33);
			this.GridLogSplitContainer.Name = "GridLogSplitContainer";
			this.GridLogSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// GridLogSplitContainer.Panel1
			// 
			this.GridLogSplitContainer.Panel1.Controls.Add(this.ContentPanel);
			// 
			// GridLogSplitContainer.Panel2
			// 
			this.GridLogSplitContainer.Panel2.Controls.Add(this.BottomPanel);
			this.GridLogSplitContainer.Size = new System.Drawing.Size(758, 510);
			this.GridLogSplitContainer.SplitterDistance = 425;
			this.GridLogSplitContainer.TabIndex = 7;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(766, 569);
			this.Controls.Add(this.GridLogSplitContainer);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.statusStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Padding = new System.Windows.Forms.Padding(4);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Manual Gondola File Push";
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.BottomPanel.ResumeLayout(false);
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MagGrid)).EndInit();
			this.GridLogSplitContainer.Panel1.ResumeLayout(false);
			this.GridLogSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridLogSplitContainer)).EndInit();
			this.GridLogSplitContainer.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel ProgressLabel;
		private System.Windows.Forms.ToolStripProgressBar ProgressBar;
		private System.Windows.Forms.FlowLayoutPanel SystemsPanel;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button FindGondolaFilesBtn;
		private System.Windows.Forms.Panel BottomPanel;
		private System.Windows.Forms.Panel ContentPanel;
		private System.Windows.Forms.DataGridView Grid;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
		private System.Windows.Forms.DataGridViewButtonColumn Column6;
		private System.Windows.Forms.ListBox LogList;
		private System.Windows.Forms.SplitContainer GridLogSplitContainer;
		private System.Windows.Forms.DataGridView MagGrid;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
	}
}

