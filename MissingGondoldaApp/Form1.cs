﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace MissingGondoldaApp
{
    public partial class MainForm : Form
    {
        class GondolaFile
        {
            public string Path;
            public string Line;
            public uint Gondola;
            public string FillStart;
            public string FillEnd;
            public uint MagCount;
            public bool Recreated;
            public Dictionary<string, string> Magazines;
        }
        List<GondolaFile> Files = new List<GondolaFile>();
		Dictionary<string, string> PPsAndPPPaths = new Dictionary<string, string>() {
			{ "PP1",    @"\\10.8.12.116\Dataexchange\Gondola" },
			{ "PP2",	@"\\10.8.12.126\Dataexchange\Gondola" },
			{ "PP3",	@"\\10.8.13.116\Dataexchange\Gondola" },
			{ "PP4",	@"\\10.8.13.126\Dataexchange\Gondola" },
			{ "PP5",	@"\\10.8.16.116\Dataexchange\Gondola" },
			{ "PP6",	@"\\10.8.16.126\Dataexchange\Gondola" },
			{ "PP7",	@"\\10.8.17.126\Dataexchange\Gondola" },
			{ "PP8",	@"\\10.8.17.116\Dataexchange\Gondola" },
			{ "PP13",	@"\\10.8.18.116\Dataexchange\Gondola" },
			{ "PP14",	@"\\10.8.18.126\Dataexchange\Gondola" },
			{ "PP15",	@"\\10.8.18.136\Dataexchange\Gondola" },
		};

		private void SetBusy(string msg) {
			ProgressLabel.Text = msg;
			ProgressBar.Style = ProgressBarStyle.Marquee;
			FindGondolaFilesBtn.Enabled = false;
		}
		private void SetDone(string msg) {
			ProgressLabel.Text = msg;
			ProgressBar.Style = ProgressBarStyle.Continuous;
			ProgressBar.Value = 0;
			FindGondolaFilesBtn.Enabled = true;
		}
		private void AddSystem(string system, bool online) {			
			var label = new Label() {
				TextAlign = ContentAlignment.MiddleCenter,
				Text = system,
				Width = 40,
				BackColor = online ? Color.LawnGreen : Color.PaleVioletRed,
				Margin = new Padding(1)
			};
			SystemsPanel.Controls.Add(label);
		}

        public MainForm()
        {
            InitializeComponent();
			Text = "Missing Gondola Files App - v1.2";

			LogLoad();
            ProgressLabel.Text = "Starting Up...";
			Log("Starting Up...");
        }

        private async void FindGondolaFiles_Click(object sender, EventArgs e)
        {
			FindGondolaFiles();
			return;

			SetBusy("Finding Gondola Files...");
			Files.Clear();
			SystemsPanel.Controls.Clear();
            var failedPPs = new List<string>(15);

#if !DEBUG
			foreach (var p in PPsAndPPPaths) {
                try {
					bool online = false;
                    await Task.Run(() => {
						online = GetHostOnline(p.Value);						

						if (online) {							
							Files.AddRange(GetGondolaFiles(p.Value));
                        }
                        else {
							failedPPs.Add(p.Key);
                        }                        
                    });
					AddSystem(p.Key, online);
				}
                catch (Exception) { }
            }
#else
			AddSystem("testing", true);
			Files.AddRange(GetGondolaFiles(@"C:\Programming\testing\GondolaFiles"));	// Test Directory
#endif

			Grid.Rows.Clear();
            foreach (var f in Files) {
                Grid.Rows.Add(
                    f.Line,
                    f.Gondola,
                    f.FillStart,
                    f.FillEnd,
                    f.MagCount,
                    f.Recreated ? "Okay" : "Push"
                    );
            }

            failedPPs.Sort();
            var MissingPPs = (failedPPs.Count < 1) ? string.Empty : string.Format("Couldn't connect to {0}!  ", string.Join(", ", failedPPs));
            if (Files.Count < 1) {                
                SetDone(string.Format("{0}No Gondola Files Found.", MissingPPs));
            } else {
                SetDone(string.Format("{0}Found {1} Gondola Files.", MissingPPs, Files.Count));
            }            
        }

		private async void FindGondolaFiles() {
			var failedPPs = new List<string>(15);

			// -- Set UI Busy
			SetBusy("Finding Gondola Files...");
			Files.Clear();
			SystemsPanel.Controls.Clear();

			// -- If we're debugging, use only our testing path
#if !DEBUG
			var Systems = PPsAndPPPaths;
#else
			var Systems = new Dictionary<string, string> {
				{ "Testing", @"C:\Programming\testing\GondolaFiles" }
			};
#endif

			// -- Get the files from each PP path. If we got any files, we're online. If not, we're offline
			var tasks = new Dictionary<string, Task<List<FileInfo>>>();
			foreach (var p in Systems) {
				tasks.Add(
						p.Key, 
						new Task<List<FileInfo>>(() => {
							return new DirectoryInfo(p.Value).GetFiles().ToList();
						})
					);
				tasks[p.Key].Start();
			}
			Task.WaitAll(tasks.Values.ToArray(), 15000);
			
			// -- Update UI for the files we found
			foreach (var p in tasks) {
				if (p.Value.IsCompleted && p.Value.Result.Count > 0) {
					AddSystem(p.Key, true);					
				} else {
					AddSystem(p.Key, false);
					failedPPs.Add(p.Key);
				}
			}

			// -- Process the files we've gotten
			foreach (var p in tasks) {
				if (p.Value.IsCompleted && p.Value.Result.Count > 0) {
					await Task.Run(() => {
						Files.AddRange(GetGondolaFiles(p.Key, p.Value.Result));
					});
				}
			}

			// -- Display the files we've found
			Grid.Rows.Clear();
			foreach (var f in Files) {
				Grid.Rows.Add(
					f.Line,
					f.Gondola,
					f.FillStart,
					f.FillEnd,
					f.MagCount,
					f.Recreated ? "Okay" : "Push"
					);
			}

			// -- Show wich PPs we coun't connect to (Or success msg)
			failedPPs.Sort();
			var MissingPPs = (failedPPs.Count < 1) ? string.Empty : string.Format("Couldn't connect to {0}!  ", string.Join(", ", failedPPs));
			if (Files.Count < 1) {
				SetDone(string.Format("{0}No Gondola Files Found.", MissingPPs));
			}
			else {
				SetDone(string.Format("{0}Found {1} Gondola Files.", MissingPPs, Files.Count));
			}
		}

		private bool GetHostOnline(string path) {
            const int timeout = 500;    // in ms

            var task = new Task<bool>(() => Directory.Exists(path));
            task.Start();
            return task.Wait(timeout) && task.Result;
        }

        private bool RecreateFile(string file)
        {
            if (MessageBox.Show("Are you sure you wish to push this file to SCADA?", "Are you sure?", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {

                // Create a copy of this file with a txt extention
                string f = file.Substring(0, file.Length - 4);
                string newfile = string.Format("{0}.txt", f);
                string backup = string.Format("{0} - Copy.tmp", f);
                try {
					ProgressLabel.Text = "Create copy of file as backup file...";
                    File.Copy(file, backup);
					ProgressLabel.Text = "Rename file so SCADA can grab it...";
                    File.Move(file, newfile);                    
                    return true;
                } catch (Exception ex) {
					var msg = string.Format("Tried to copy file, but got an error:\n{0}", ex.Message);
					Log(msg);
					MessageBox.Show(msg);
                    return false;
                }                
            }
            return false;
        }

		private string GetPPFromPath(string Path) {
			var v = PPsAndPPPaths.Where(x => x.Value == Path).Select(x => x.Key);
			if (v.Count() > 0) return v.First();
			return "?";

			// Old way
			//if (Path.Contains("ALUSAT-W75041"))
			//	return "PP1";
			//if (Path.Contains("ALUSAT-W75043"))
			//	return "PP2";
			//if (Path.Contains("ALUSAT-W75059"))
			//	return "PP3";
			//if (Path.Contains("ALUSAT-W75028"))
			//	return "PP4";
			//if (Path.Contains("ALUSAT-W76107"))
			//	return "PP5";
			//if (Path.Contains("ALUSAT-W76111"))
			//	return "PP6";
			//if (Path.Contains("ALUSAT-W76044"))
			//	return "PP8";
			//if (Path.Contains("ALUSAT-W76048"))
			//	return "PP7";
			//if (Path == "test")
			//	return "PP99";
			//return "?";
		}

		private List<GondolaFile> GetGondolaFiles(string path)
        {
			return GetGondolaFiles(GetPPFromPath(path), new DirectoryInfo(path).GetFiles().ToList());
        }
		private List<GondolaFile> GetGondolaFiles(string line, List<FileInfo> fileinfos) {
			var files = fileinfos.Where(f => f.CreationTime >= DateTime.Now.Date.AddDays(-2) && f.Length > 120)
								 .OrderByDescending(f => f.CreationTime).Select(f => f.FullName);

			List<GondolaFile> gondolas = new List<GondolaFile>();

			foreach (var f in files) {
				if (f.EndsWith("tmp")) {
					var lines = File.ReadAllLines(f);
					if (lines.Count() < 5) continue;            // Less than 5 lines isn't a good file
					if (f.EndsWith(" - Copy.tmp")) continue;    // Skip files we can't do anything with

					GondolaFile gf = new GondolaFile() {
						Line = line,
						Path = f,
						Gondola = uint.Parse(lines[0]),
						FillStart = lines[2],
						FillEnd = lines[3],
						MagCount = uint.Parse(lines[4]),
						Recreated = Path.GetFileNameWithoutExtension(f).EndsWith(" - Copy"),
						Magazines = new Dictionary<string, string>(lines.Count() - 4)
					};
					for (uint i = 5; i < lines.Count(); i += 2) {
						string mag = lines[i];
						string lot = lines[i + 1];

						if (string.IsNullOrWhiteSpace(mag) || string.IsNullOrWhiteSpace(lot))
							break;

						gf.Magazines.Add(mag, lot);
					}

					gondolas.Add(gf);
				}
			}

			return gondolas;
		}

		private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Grid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                    e.RowIndex >= 0 && e.RowIndex < Files.Count) {

				PushGondolaFile(e.RowIndex);           
            }
        }
		private void PushGondolaFile(int row) {
			SetBusy("Pushing Gondola File...");
			if (!Files[row].Recreated) {
				if (RecreateFile(Files[row].Path)) {
					//(Grid.Rows[row].Cells[5] as DataGridViewButtonCell).Value = "Done";
					Grid.ClearSelection();	// Prevent the magazine data from going stale
					Grid.Rows.RemoveAt(row);
					Log("Recreated Gondola #{0} from {1} successfully.", Files[row].Gondola, Files[row].Line);
					SetDone(string.Format("Recreated Gondola #{0} from {1} successfully.", Files[row].Gondola, Files[row].Line));
				}
				else {
					Log("Error recreated Gondola #{0} from {1}.", Files[row].Gondola, Files[row].Line);
					SetDone(string.Format("Error recreating Gondola #{0} from {1}.", Files[row].Gondola, Files[row].Line));
				}
			}
		}

        private void Grid_SelectionChanged(object sender, EventArgs e)
        {
			MagGrid.Rows.Clear();
            if (Grid.SelectedRows.Count > 0 && Grid.Rows.Count > 0 && Grid.SelectedRows[0].Index >= 0 && Grid.SelectedRows[0].Index < Files.Count) {
                uint i = 1;
                foreach (var m in Files[Grid.SelectedRows[0].Index].Magazines) {
                    //MagListBox.Items.Add(string.Format("{0:00}: {1} (Lot# {2})", i++, m.Key, m.Value));
					MagGrid.Rows.Add(m.Key.Trim().ToUpper(), m.Value.Trim().ToUpper());
                }
            }
        }

		private void Log(string s, params object[] vars) {
			var toLog = string.Format(s, vars);
			toLog = string.Format("{0} - {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), toLog);
			// TODO persist to file?

			LogList.Items.Insert(0, toLog);
		}
		private void LogLoad() {
			// TODO persist log?

			LogList.Items.Clear();
		}

		private void MagGrid_KeyDown(Object sender, KeyEventArgs e) {
			// -- Compare clipboard to Magazines (Don't parse entire gondola form)
			if (e.Control && e.KeyCode == Keys.V && Clipboard.ContainsText()) {
				HighlightMagazines(Clipboard.GetText());				
			}
		}
		private bool HighlightMagazines(string text) {
			// Clear the selection so we know if we highlight cells, they'll be visible
			MagGrid.ClearSelection();

			// Format text (It probably came from the clipboard)
			var s = text.Replace("\r", string.Empty).Split('\n');

			// Build a Hash of the clipboard contents
			var parsedList = new Dictionary<string, string>();

			// Parse each line into dictionary
			foreach (var line in s) {
				if (string.IsNullOrWhiteSpace(line)) continue;
				var split = line.Split('\t');
				if (split.Length == 4 && !string.IsNullOrWhiteSpace(split[2]) && !string.IsNullOrWhiteSpace(split[3])) {
					parsedList.Add(split[2].Trim().ToUpper(), split[3].Trim().ToUpper());
				}
				if (split.Length == 2 && !string.IsNullOrWhiteSpace(split[0]) && !string.IsNullOrWhiteSpace(split[1])) {
					parsedList.Add(split[0].Trim().ToUpper(), split[1].Trim().ToUpper());
				}
			}

			// Now check if each row exists in the dictionary with the same lot #
			var allMatch = true;
			foreach (var row in MagGrid.Rows) {
				var magCell = (row as DataGridViewRow).Cells[0];
				var lotCell = (row as DataGridViewRow).Cells[1];

				if (parsedList.ContainsKey(magCell.Value.ToString())) {
					magCell.Style.BackColor = Color.LawnGreen;

					if (parsedList[magCell.Value.ToString()] == lotCell.Value.ToString()) {
						lotCell.Style.BackColor = Color.LawnGreen;
					}
					else {
						lotCell.Style.BackColor = Color.PaleVioletRed;
						allMatch = false;
					}
				}
				else {
					magCell.Style.BackColor = Color.PaleVioletRed;
					lotCell.Style.BackColor = Color.PaleVioletRed;
					allMatch = false;
				}
			}

			return allMatch;
		}

		private void Grid_KeyDown(Object sender, KeyEventArgs e) {
			// -- Compare clipboard to Magazines and Gondola (Entire gondola file)
			if (e.Control && e.KeyCode == Keys.V && Clipboard.ContainsText()) {

				// Build a Hash of the clipboard contents
				var parsedList = new Dictionary<string, string>();
				var gondola = string.Empty;
				var magSection = -1;
				var s = Clipboard.GetText().Replace("\r", string.Empty).Split('\n');

				// -- Find Gondola #
				for (int i = 0; i < s.Length; i++) {
					if (s[i].Contains("Gondola Number")) {
						gondola = s[i + 1].Trim();
					}
					else if (s[i].Contains("Sequence #") || s[i].Contains("FG Batch Numbers ")) {
						magSection = i + 1;
					}

					if (gondola != string.Empty && magSection > -1) break;
				}
				if (gondola == string.Empty) {
					ProgressLabel.Text = "Gondola # not found in pasted text.";
					HighlightMagazines(Clipboard.GetText());
					return;
				}

				// -- Select the gondola row, if we have it
				for (int i = 0; i < Files.Count; i++) {
					if (Files[i].Gondola == uint.Parse(gondola)) {
						Grid.Rows[i].Selected = true;

						// Set the magazine text to help the highlighting
						var text = string.Join("\n", s.Skip(magSection));
						var matches = HighlightMagazines(text);

						// Inform user that gondola matches
						ProgressLabel.Text = string.Format("Pasted Gondola {0} {1}", gondola, matches ? "matches, Ready to Push!" : "does not match!!");

						// TODO, do we want to go ahead and ask to push file if we pasted okay? 
						//if (matches) PushGondolaFile(i);
						return;
					}
				}

				// -- No gondola found
				ProgressLabel.Text = string.Format("Pasted Gondola {0} not found", gondola);
			}
		}

		private void MainForm_Shown(Object sender, EventArgs e) {
			FindGondolaFiles();
		}
	}
}
